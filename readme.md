# USI Verification Script

Verification Script for USI system using Mink (Browser Simulation)

## Quick Start Quide

- Download Selenium Server from https://www.seleniumhq.org/download/ , and put it in project folder.
- Make sure your WAMP (or MAMP) has been turned on. The script can be running directly from localhost.
- Open CMD on Windows (or terminal in MAC/LINUX) , CD to the project folder
- Run "java -jar selenium-server-standalone-3.9.1" (or similar line on MAC/LINUX) PS： "java -jar 'selenium-server-standalone-3.9.1.jar' " on Windows 7
- Open Browser , set it to "localhost/{your-project-folder}"

## CRON/TASK Schedule Setting

By running the script once, it will get the list of Unverified students, verifiy them and send the results back to LMS system.
In order to keep this procedure automated, one solution is to set this to be running on schedule few times a day. 
If you are using Windows , TASK SCHEDULER is a nice tool to achieve this. Here is a nice tutorial for that : https://www.technig.com/windows-10-task-scheduler/

## Potential Issues.

If the script doesn't work or run into errors , here is some potential reasons:

- The browser simulation is based on CSS tag mostly , so if the page structure for USI verification page (https://portal.usi.gov.au/org/Usi/Verify) has changed, it might cause this script to be not working.
- It's using Chrome browser driver located in "C:\chromedriver_win32". The driver version might be updated frequently. Same as the Chrome version. So updates for chrome driver and chrome version might be needed.
- Chrome simulation using AUSkey extension located in "\vendor\behat\mink-selenium2-driver\src\bin". Update for this extension might be needed.

