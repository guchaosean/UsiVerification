<?php

require_once 'vendor/autoload.php';

use Behat\Mink\Mink,
    Behat\Mink\Session,
    Behat\Mink\Driver\Selenium2Driver;

class UsiVerifier {

    private $username;
    private $password;

    public function __construct($username, $password) {
        $this->username = $username;
        $this->password = $password;
    }

    private function formatObject($usi, $result, $messages) {
        $object = [
            'usi' => $usi,
            'result' => $result,
            'feedback' => $messages,
        ];
        return $object;
    }

    private function formatMessage($messages) {
        $string = "";
        foreach ($messages as $message) {
            $string = trim($string . $message->getHtml());
        }
        return $string;
    }

    public function verifyUsi($students) {
        ini_set('max_execution_time', 300);

        //Initiate and Redirect 
        $realUrl = 'https://portal.usi.gov.au/org/Account/Login';
        $mink = new Mink(array(
            'seleium2' => new Session(new Selenium2Driver('chrome')),
        ));
        $mink->setDefaultSessionName('seleium2');
        $mink->getSession()->visit($realUrl);

        //Choose key and enter password
        $select = $mink->getSession()->getPage()->find('named', array('select', 'ddlCombo'));
        $select->selectOption($this->username);
        $input = $mink->getSession()->getPage()->find('named', array('id', 'txtPassword'));
        $input->setValue($this->password);
        $mink->getSession()->getPage()->find('named', array('id', 'imgContinue'))->click();

        //Wait for 3 seconds for page to load
        $mink->getSession()->wait(3000);

        //agree to terms
        $mink->getSession()->getPage()->find('named', array('checkbox', 'AgreedToTermsAndConditions'))->check();
        $mink->getSession()->getPage()->find('named', array('id', 'nextButton'))->click();

        //Redirect to verify page
        $mink->getSession()->getPage()->find('named', array('link', 'Verify USI'))->click();

        $object = [];

        foreach ($students as $student) {

            //Input Info
            $mink->getSession()->getPage()->find('named', array('id', 'Usi'))->setValue($student->usi);
            $mink->getSession()->getPage()->find('named', array('id', 'FirstName'))->setValue($student->first_name);
            $mink->getSession()->getPage()->find('named', array('id', 'FamilyName'))->setValue($student->family_name);

            $day = date('j', strtotime($student->date_of_birth));
            $month = date('n', strtotime($student->date_of_birth)) - 1;
            $year = date('Y', strtotime($student->date_of_birth));
            
            $mink->getSession()->getPage()->find('css', '.year')->setValue($year);
            $mink->getSession()->getPage()->find('css', '.month')->setValue($month);
            $mink->getSession()->getPage()->find('css', '.day')->setValue($day);

            //Click Verify
            $mink->getSession()->getPage()->find('css', '#button-container>input:first-child')->click();

            //Get the messages and put it in object
            $message = $mink->getSession()->getPage()->find('css', '.user-msgs > div');

            if ($message->hasClass('warning')) {

                $warnings = $message->findAll('css', 'ul>li');
                array_push($object, $this->formatObject($student->usi, 0, $this->formatMessage($warnings)));
            } else {

                $successes = $message->findAll('css', 'ul>li');
                array_push($object, $this->formatObject($student->usi, 1, $this->formatMessage($successes)));
            }
        }

        //Stop the session
        $mink->getSession()->stop();
        return $object;
    }

    public function run() {
        $list = $this->getList();
        //If no students find , exit
        if (!isset($list)) {
            exit;
        }
        $object = $this->verifyUsi($list);
        $this->postResult($object);
    }

    private function postResult($object) {
        $client = new \GuzzleHttp\Client;

        $response = $client->request('POST', 'http://hia.energe3.com.au/lms/userUsi/submitResults', [
            'form_params' => [
                'key' => 'XRnpvoGJTzMFnyVw8WZ7',
                'results' => $object
            ]
        ]);

        var_dump($response->getBody()->getContents());
    }

    private function getList() {
        $client = new \GuzzleHttp\Client;

        $response = $client->request('POST', 'http://hia.energe3.com.au/lms/userUsi/verificationList', [
            'form_params' => [
                'key' => 'XRnpvoGJTzMFnyVw8WZ7'
            ]
        ]);

        return json_decode($response->getBody()->getContents());
    }

}

?>